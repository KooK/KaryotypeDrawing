#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : generate a schema representing a karyotype.
## USAGE : ./karyotype.py --infile X --outfile Y --format png
## USAGE : cat X | ./karyotype.py > Y.png
## AUTHORS : KooK.dev@free.fr


import sys
import pdb	# debug
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os	# for local libraries
import csv	# manage tsv files also.

from Bio.Graphics import BasicChromosome

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../lib" )

VERSION = "0.1"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE   = False

# ========================================
def main( o_args ):
	verbose( "infile  = {0}\noutfile = {1}".format(
				str(o_args.infile), str( o_args.outfile ) ) )
				
	l_chrom_sizes = list( load_infile( o_args.infile ) )
	diag = compute_diagram( l_chrom_sizes, o_args.format )
	diag.draw( o_args.outfile, o_args.title )

	return


def load_infile( infile ):
	#~ x = map( split, infile.readlines() )
	#~ PP.pprint( x )

	import csv
	parser = csv.reader( infile, delimiter="\t" )
	for chrom_id,length in parser:
		yield [ chrom_id, int( length ) ]
# load_infile


def compute_diagram( l_chrom_sizes, out_format ):
	max_chrom = max( l_chrom_sizes, key=lambda x:x[1] )
	verbose( "Chrom max :" + str( max_chrom ) )

	chr_diagram = BasicChromosome.Organism()
	if( out_format ):
		chr_diagram.output_format = out_format

	# biopython force to define telomere region
	#	telomere should be included in chrom length.
	#	So i'll use a fake 1 base length for those regions
	telomere_length = 1

	for label,length in l_chrom_sizes:
		verbose( label + " => " + str( length ) )
		cur_chrom = BasicChromosome.Chromosome( label )
		cur_chrom.scale_num = max_chrom[1]

		# Add an opening telomere
		start = BasicChromosome.TelomereSegment()
		start.scale = telomere_length
		cur_chrom.add( start )

		# Add a body - using bp as the scale length here.
		body = BasicChromosome.ChromosomeSegment()
		body.scale = length - 2 * telomere_length
		cur_chrom.add( body )

		# Add an closing telomere
		end = BasicChromosome.TelomereSegment( inverted=True )
		end.scale = telomere_length
		cur_chrom.add( end )

		chr_diagram.add( cur_chrom )

	return chr_diagram
# compute_diagram

# ----------------------------------------
def defineOptions():
	o_parser = argparse.ArgumentParser( description='Prog descr.' )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	#	type = int, file

	# optionnal
	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )

	o_parser.add_argument( '--infile', '-i',
							type=argparse.FileType( 'r' ),
							default=sys.stdin,
							help='The tsv file containing chrom length (2 cols: id, length). Default is stdin.' )
	o_parser.add_argument( '--outfile', '-o',
							type=test_file('wb'),
							help='outfile, default is stdout.' )

	o_parser.add_argument( '--format', '-f',
							type=str,
							help='the file format of the output diagram. default output is based on the extension of the outfile name, if stdin, format is png.' )
	o_parser.add_argument( '--title', '-t',
							type=str, default="",
							help='the title of the diagram.' )

	return o_parser

def test_file(mode):
	"""Decorator that detects if read/write mode of given files is possible
		use: test_file(mode) where mode can be 'r', 'rb', 'w', 'wb'."""

	def _file(filepath):
		"""Argparse type, raising an error if given file does not exists"""
		if ('r' in mode) and not os.access(filepath, os.R_OK):
			raise argparse.ArgumentTypeError("file {} doesn't exists".\
													format(filepath))
		elif ('w' in mode) and not os.access(filepath, os.W_OK):
			raise argparse.ArgumentTypeError("file {} is not writable".\
													format(filepath))
		return filepath

	return _file


def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True

	if( not o_args.format ):
        # Detect format from filename if not given,
		if( str == type( o_args.outfile ) ):
			o_args.format = o_args.outfile.rsplit( ".", 1 )[-1]
		else:
			o_args.format = 'png'

	if not o_args.outfile:
		# Workaround for a bug in reportlab svg render module in Python3:
		# f.write(svg if isPy3 else svg.encode(self.encoding))
		# TypeError: a bytes-like object is required, not 'str'
		if o_args.format == 'svg' or (sys.version_info.major == 2):
			o_args.outfile = sys.stdout
		else:
			o_args.outfile = sys.stdout.buffer

def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	#~ PP.pprint( o_args )

	main( o_args )

	sys.exit( 0 )

